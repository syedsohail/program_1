# PS-get-deep-property exercise
 
## Instructions
 
- Using vanilla javascript, create a function that gets the value of a property at a given path
- Example:
  - If given the object: {person: {name: {first: 'FirstName', middleInitial: 'I', lastName: 'LastName''}}}
  - And given the path: 'person.name.lastName'
  - The output would be: 'LastName'
- After you complete the exercise, provide any notes on your code below such as how to run your example
 
## Candidate Notes:
 
var data = {person: {name: {first: 'FirstName', middleInitial: 'I', lastName: 'LastName'}}};
var path = 'person.name.lastName';
 
var getValue = function(data, path){
    for (var i=0, path=path.split('.'), len=path.length; i<len; i++){
        data = data[path[i]];
    };
    return data;
};
 
getValue(data,path);
 